function [output_sig] = radar_echo(sig_chirp)
%RADAR_ECHO Summary of this function goes here
%   Funkcja zwraca sygnal odebrany przez radar po wyslaniu przez
%   niego impulsu (bardzo uproszczony model)
len = length(sig_chirp);
delta_sample = round(15*len*rand())+5*len;
output_sig = cat(2, zeros(1, delta_sample), sig_chirp);
output_sig = output_sig + 0.05*randn(1,length(output_sig));
end

